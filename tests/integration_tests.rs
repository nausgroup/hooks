use hooks;
use std::sync::Arc;

#[derive(Default)]
struct TestEvent {
	foo : u8
}

#[test]
fn publish_and_retrieve_event() {
	let mut core = Arc::new(hooks::Core::<TestEvent>::new(64).unwrap());
	let mut writer = hooks::Writer::new(&core).unwrap();
	let mut reader = hooks::Reader::new(&core);

	writer.get().foo = 5;
	let _ = writer.commit();
	let rcv_ev = reader.next().unwrap();

	assert_eq!(rcv_ev.foo, 5);
}
