//! Hooks
//!
//! `hooks` is an event dispatcher loosely adapted from LMAX's Disruptor.

use std::{sync::{Arc, Mutex, RwLock, atomic::{ AtomicUsize, Ordering }}, usize};
use std::cell::UnsafeCell;

pub struct Core<T> where T : Default {
	ring : Vec<UnsafeCell<T>>,
	has_issued_writer : Mutex<bool>,
	mod_mask : usize,
	sequence : AtomicUsize,
	reader_sequences : RwLock<Vec<Arc<AtomicUsize>>>
}

pub struct Writer<T> where T : Default {
	core : Arc<Core<T>>
}

pub struct Reader<T> where T : Default {
	core : Arc<Core<T>>,
	sequence : Arc<AtomicUsize>
}

impl<T> Core<T> where T : Default {
	
	pub fn new(len : usize) -> Result<Self, ()> {
		// ring length must be a (positive) power of two
		if len > 0 && len & (len - 1) == 0 { 
			let mut ret = Self { 	ring : Vec::with_capacity(len),
											has_issued_writer : Mutex::new(false),
											mod_mask : len - 1,
											sequence : 0.into(),
											reader_sequences : RwLock::new(Vec::new()) };
			ret.ring.resize_with(len, Default::default);
			
			Ok(ret)
		} else {
			Err(())
		}
	}

	fn len(&self) -> usize {
		self.ring.len()
	}

	fn get(&self, sequence : usize) -> &UnsafeCell<T> {
		self.ring.get(sequence & self.mod_mask).unwrap()
	}
}

impl<T> Writer<T> where T : Default {
	
	pub fn new(core : &Arc<Core<T>>) -> Result<Writer<T>, ()> {
		let mut has_issued_writer = core.has_issued_writer.lock().unwrap();
		if !*has_issued_writer {
			*has_issued_writer = true;
			Ok(Writer::<T> { core : core.clone() })
		} else {
			Err(())
		}
	}

	pub fn get(&mut self) -> &mut T {
		let ptr = self.core.get(self.core.sequence.load(Ordering::SeqCst)).get();
		unsafe {
			&mut *ptr
		}
	}

	pub fn commit(&mut self) -> Result<(), ()> {
		let min_sequence = self.core.reader_sequences.read().unwrap().iter().min_by(|x, y| {
			x.load(Ordering::SeqCst).cmp(&y.load(Ordering::SeqCst))
		}).unwrap().load(Ordering::SeqCst);

		if self.core.sequence.load(Ordering::SeqCst) < min_sequence + self.core.len() {
			self.core.sequence.fetch_add(1, Ordering::SeqCst);
			Ok(())
		} else {
			Err(())
		}
	}
}

impl<T> Reader<T> where T : Default {

	pub fn new(core : &Arc<Core<T>>) -> Reader<T> {
		let ret = Reader { core : core.clone(), sequence : Arc::new(AtomicUsize::new(core.sequence.load(Ordering::SeqCst))) };
		core.reader_sequences.write().unwrap().push(ret.sequence.clone());
		ret
	}

	pub fn next(&mut self) -> Option<&T> {
		if self.sequence.load(Ordering::SeqCst) < self.core.sequence.load(Ordering::SeqCst) {
			let ptr = self.core.get(self.sequence.load(Ordering::SeqCst)).get();
			self.sequence.fetch_add(1, Ordering::SeqCst);

			unsafe {
				Some(&*ptr)
			}
			
		} else {
			None
		}
	}
}

#[cfg(test)]
mod tests {

	use super::*;

	#[test]
	fn cannot_create_zero_length_ring() {
		assert!(Core::<u8>::new(0).is_err());
	}

	#[test]
	fn length_must_be_power_of_two() {
		assert!(Core::<u8>::new(10).is_err());
	}

	#[test]
	// Single Producer Multiple Consumers (spmc) for now
	fn cannot_have_multiple_writers() {
		let core = Arc::new(Core::<u8>::new(16).unwrap());

		Writer::new(&core);
		assert!(Writer::new(&core).is_err());
	}

	#[test]
	fn writer_cannot_overtake_slowest_reader() {
		let core = Arc::new(Core::<u8>::new(8).unwrap());

		let mut writer = Writer::new(&core).unwrap();
		let mut reader = Reader::new(&core);

		for i in 0..8 {
			writer.commit();
		}

		assert!(writer.commit().is_err());
	}

	#[test]
	fn cannot_read_when_ring_is_empty() {
		let core = Arc::new(Core::<u8>::new(16).unwrap());

		let mut reader = Reader::new(&core);
		assert!(reader.next().is_none());
	}
}
